<?php

if( ! class_exists('Application') ){
    require_once dirname(APP_FILE) . '/vendor/autoload.php';

    class Application {
        private $twig = null;

        function __construct() {
            $loader = new \Twig\Loader\FilesystemLoader( dirname(APP_FILE) .  '/templates');
            $this->twig = new \Twig\Environment($loader, [
                'cache' => dirname(APP_FILE) .  '/cache',
            ]);
        }

        public function render($template, array $data = []) {
            $template = $this->twig->load($template);
            return $template->render($data);
        }
    }

}
