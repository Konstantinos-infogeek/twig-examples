<?php
    if(!defined('APP_FILE')){ define('APP_FILE', __FILE__); }
    include __DIR__ . '/application/Application.php';

    $app = new Application();
    echo $app->render('index.twig');
?>